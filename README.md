# raspberrypi_utils

Utilities for shrinking, extracting, and re-expanding the second partition on a USB drive or SD card.